package co.com.proyectobase.screenplay02.interactions;

import java.util.List;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;

import static co.com.proyectobase.screenplay02.userinterface.AddEmployeePage.*;

public class SelectLocation implements Interaction {

	private Target list;
	private String option;

	public SelectLocation(Target list, String option) {
		this.list = list;
		this.option = option;
	}

	public static SelectLocation theList(Target list, String option) {
		return new SelectLocation(list, option);
	}

	@Override
	public <T extends Actor> void performAs(T actor) {

		actor.attemptsTo(Click.on(SELCT_LOCATION));

		List<WebElementFacade> listObject = list.resolveFor(actor).thenFindAll(By.tagName("li"));

		for (WebElementFacade item : listObject) {
			if (item.isCurrentlyVisible()) {
				if (item.getText().trim().equals(option.trim())) {
					actor.attemptsTo(Click.on(item));
					break;
				}

			}
		}
	}

}
