package co.com.proyectobase.screenplay02.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import co.com.proyectobase.screenplay02.userinterface.LoginPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

public class OpenWeb implements Task {

	private LoginPage loginPage;

	public static OpenWeb theLoginPage() {
		return instrumented(OpenWeb.class);
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(loginPage));
	}

}
