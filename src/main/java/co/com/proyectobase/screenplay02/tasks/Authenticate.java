package co.com.proyectobase.screenplay02.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import co.com.proyectobase.screenplay02.model.User;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static co.com.proyectobase.screenplay02.userinterface.LoginPage.*;

public class Authenticate implements Task {

	private User user;

	public Authenticate(User user) {
		this.user = user;
	}

	public static Authenticate the(User user) {
		return instrumented(Authenticate.class, user);
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
				Enter.theValue(user.getUsername()).into(TEXT_USERNAME),
				Enter.theValue(user.getPassword()).into(TEXT_PASSWORD), 
				Click.on(BUTTON_LOGIN));
	}
}
