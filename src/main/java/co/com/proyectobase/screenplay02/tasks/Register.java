package co.com.proyectobase.screenplay02.tasks;

import static co.com.proyectobase.screenplay02.userinterface.AddEmployeePage.BUTTON_SAVE;
import static co.com.proyectobase.screenplay02.userinterface.AddEmployeePage.OPTION_PIM;
import static co.com.proyectobase.screenplay02.userinterface.AddEmployeePage.OPTION_PIM_ADD_EMPLOYEE;
import static co.com.proyectobase.screenplay02.userinterface.AddEmployeePage.SELCT_LOCATION_OPTIONS;
import static co.com.proyectobase.screenplay02.userinterface.AddEmployeePage.TEXT_FIRSTNAME;
import static co.com.proyectobase.screenplay02.userinterface.AddEmployeePage.TEXT_LASTNAME;
import static co.com.proyectobase.screenplay02.userinterface.AddEmployeePage.TEXT_MIDDLETNAME;
import static net.serenitybdd.screenplay.Tasks.instrumented;

import co.com.proyectobase.screenplay02.interactions.SelectLocation;
import co.com.proyectobase.screenplay02.model.Employee;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Register implements Task {

	private Employee employee;

	public Register(Employee employee) {
		this.employee = employee;
	}

	public static Register the(Employee employee) {
		return instrumented(Register.class, employee);
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(
				Click.on(OPTION_PIM),
				Click.on(OPTION_PIM_ADD_EMPLOYEE));
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		actor.attemptsTo(
				Enter.theValue(employee.getFirstName()).into(TEXT_FIRSTNAME),
				Enter.theValue(employee.getMiddleName()).into(TEXT_MIDDLETNAME),
				Enter.theValue(employee.getLastName()).into(TEXT_LASTNAME),
				SelectLocation.theList(SELCT_LOCATION_OPTIONS, employee.getLocation()),
				Click.on(BUTTON_SAVE));
	}

}
