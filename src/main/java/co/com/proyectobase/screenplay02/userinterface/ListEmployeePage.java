package co.com.proyectobase.screenplay02.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class ListEmployeePage extends PageObject {

	public final static Target TEXT_FILTER_NAME = Target.the("employee name filter field")
			.located(By.id("employee_name_quick_filter_employee_list_value"));

	public final static Target BUTTON_FILTER = Target.the("the button filter name")
			.located(By.xpath("//*[@id='right-side']/header/div/nav/div/div/ul[2]/li[2]"));

	public final static Target ROW_LIST_EMPLOYEE = Target.the("the result of filter")
			.located(By.xpath("//*[@id='employeeListTable']/tbody/tr[1]"));

	public final static Target OPTION_LIST_EMPLOYEE = Target.the("the list employee option")
			.located(By.id("menu_pim_viewEmployeeList"));

}
