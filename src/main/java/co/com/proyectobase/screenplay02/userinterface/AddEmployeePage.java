package co.com.proyectobase.screenplay02.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class AddEmployeePage extends PageObject {

	public final static Target OPTION_PIM = Target.the("the PIM option").located(By.id("menu_pim_viewPimModule"));

	public final static Target OPTION_PIM_ADD_EMPLOYEE = Target.the("the PIM option")
			.located(By.id("menu_pim_addEmployee"));

	public final static Target TEXT_FIRSTNAME = Target.the("the first name field").located(By.id("firstName"));
	public final static Target TEXT_MIDDLETNAME = Target.the("the middle name field").located(By.id("middleName"));
	public final static Target TEXT_LASTNAME = Target.the("the last name field").located(By.id("lastName"));
	public final static Target SELCT_LOCATION = Target.the("the location field")
			.located(By.xpath("//*[@id='location_inputfileddiv']/div/input"));

	public final static Target SELCT_LOCATION_OPTIONS = Target.the("the location options")
			.located(By.xpath("//*[@id='location_inputfileddiv']/div/ul"));

	public final static Target BUTTON_SAVE = Target.the("the button save").located(By.id("systemUserSaveBtn"));

}
