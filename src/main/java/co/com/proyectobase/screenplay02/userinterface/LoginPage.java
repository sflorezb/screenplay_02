package co.com.proyectobase.screenplay02.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://orangehrm-demo-6x.orangehrmlive.com/")
public class LoginPage extends PageObject {

	public final static Target TEXT_USERNAME = Target.the("the username field").located(By.id("txtUsername"));

	public final static Target TEXT_PASSWORD = Target.the("the password field").located(By.id("txtPassword"));

	public final static Target BUTTON_LOGIN = Target.the("the login button").located(By.id("btnLogin"));

}
