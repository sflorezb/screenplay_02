package co.com.proyectobase.screenplay02.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import static co.com.proyectobase.screenplay02.userinterface.ListEmployeePage.*;

public class TheEmployee implements Question<Boolean> {

	private String firstName;

	public TheEmployee(String firstName) {
		this.firstName = firstName;
	}

	public static TheEmployee isVisible(String firstName) {
		return new TheEmployee(firstName);
	}

	@Override
	public Boolean answeredBy(Actor actor) {

		try {
			Thread.sleep(9000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		actor.attemptsTo(Click.on(OPTION_LIST_EMPLOYEE), Enter.theValue(firstName).into(TEXT_FILTER_NAME),
				Click.on(BUTTON_FILTER));
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return ROW_LIST_EMPLOYEE.resolveFor(actor).isVisible();
	}

}
