package co.com.proyectobase.screenplay02.model.builder;

import co.com.proyectobase.screenplay02.model.Employee;
import co.com.proyectobase.screenplay02.util.Builder;

public class EmployeeBuilder implements Builder<Employee> {

	private String firstName;
	private String middleName;
	private String lastName;
	private String employeeId;
	private String location;

	public EmployeeBuilder(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public String getLocation() {
		return location;
	}

	public static EmployeeBuilder withFirstName(String firstName) {
		return new EmployeeBuilder(firstName);
	}

	public EmployeeBuilder withMiddleName(String middleName) {
		this.middleName = middleName;
		return this;
	}

	public EmployeeBuilder withLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public Employee withLocation(String location) {
		this.location = location;
		return this.build();
	}

	@Override
	public Employee build() {
		return new Employee(this);
	}

}
