package co.com.proyectobase.screenplay02.model;

import co.com.proyectobase.screenplay02.model.builder.UserBuilder;

public class User {

	private final String username;
	private final String password;

	public User(UserBuilder builder) {
		this.username = builder.getUsername();
		this.password = builder.getPassword();
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

}
