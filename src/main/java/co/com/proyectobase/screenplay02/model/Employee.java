package co.com.proyectobase.screenplay02.model;

import co.com.proyectobase.screenplay02.model.builder.EmployeeBuilder;

public class Employee {

	private final String firstName;
	private final String middleName;
	private final String lastName;
	private String location;

	public Employee(EmployeeBuilder employeeBuilder) {
		this.firstName = employeeBuilder.getFirstName();
		this.middleName = employeeBuilder.getMiddleName();
		this.lastName = employeeBuilder.getLastName();
		this.location = employeeBuilder.getLocation();
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public String getLastName() {
		return lastName;
	}
}
