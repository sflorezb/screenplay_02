package co.com.proyectobase.screenplay02.model.builder;

import co.com.proyectobase.screenplay02.model.User;
import co.com.proyectobase.screenplay02.util.Builder;

public class UserBuilder implements Builder<User> {

	private String username;
	private String password;

	public UserBuilder(String username) {
		this.username = username;
	}

	public static UserBuilder withUserName(String username) {
		return new UserBuilder(username);
	}

	public User withPassWrod(String password) {
		this.password = password;
		return build();
	}

	@Override
	public User build() {
		return new User(this);
	}

	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return username;
	}

}
