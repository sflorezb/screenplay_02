package co.com.proyectobase.screenplay02.util;

public interface Builder<T> {

	T build();
}