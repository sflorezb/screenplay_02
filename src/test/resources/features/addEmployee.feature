Feature: Add employee in OrangeHRM
  As user
  I want to enter to website  orangehrm
  To add employee

  Scenario Outline: Add employee
    Given that Juan wants add a employee
    And he login with usermane <Username> and password <Password>
    When he makes the registration on the page with the data
      | FirstName   | MiddleName   | LastName   | Location   |
      | <FirstName> | <MiddleName> | <LastName> | <Location> |
    Then he should see the employee <FirstName> on the app

    Examples: 
      | FirstName | MiddleName | LastName | Location               | Username | Password |
      | Sebastian | Juan       | Florez   | Australian Regional HQ | admin    | admin123 |
