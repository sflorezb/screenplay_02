package co.com.proyectobase.screenplay02.stepdefinitions;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay02.model.builder.EmployeeBuilder;
import co.com.proyectobase.screenplay02.model.builder.UserBuilder;
import co.com.proyectobase.screenplay02.questions.TheEmployee;
import co.com.proyectobase.screenplay02.tasks.Authenticate;
import co.com.proyectobase.screenplay02.tasks.OpenWeb;
import co.com.proyectobase.screenplay02.tasks.Register;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class AddEmployeeStepDefinition {

	@Managed(driver = "chrome")
	private WebDriver hisBrowser;
	private Actor juan = Actor.named("Juan");

	@Before
	public void configuracionInicial() {
		juan.can(BrowseTheWeb.with(hisBrowser));
	}

	@Given("^that Juan wants add a employee$")
	public void thatJuanWantsAddAEmployee() {
		juan.wasAbleTo(OpenWeb.theLoginPage());
	}

	@Given("^he login with usermane (.*) and password (.*)$")
	public void heLoginWithUsermaneAdminAndPasswordAdmin(String username, String password) {
		juan.wasAbleTo(Authenticate.the(UserBuilder.withUserName(username).withPassWrod(password)));
	}

	@When("^he makes the registration on the page with the data$")
	public void heMakesTheRegistrationOnThePageWithTheData(DataTable registerData) {
		List<Map<String, String>> data = registerData.asMaps(String.class, String.class);
		juan.attemptsTo(
				Register.the(
						EmployeeBuilder.withFirstName(data.get(0).get("FirstName"))
						.withMiddleName(data.get(0).get("MiddleName"))
						.withLastName(data.get(0).get("LastName"))
						.withLocation(data.get(0).get("Location")))
		);
	}

	@Then("^he should see the employee (.*) on the app$")
	public void heShouldSeeTheEmployeeOnTheApp(String firstName) {
		 juan.should(GivenWhenThen.seeThat(TheEmployee.isVisible(firstName)));
	}

}